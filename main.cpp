#include <iostream>
#include <vector>
#include <stdlib.h>
#include "account/AccountManager.h"
#include "utils/Utils.h"
#include "utils/State.h"

#include "main.h"

using namespace std;

bool isRunning = true;

int main(int argc, char** argv) {
    applicationStateHandler();

	return 0;
}

void applicationStateHandler() {
    AccountManager accountManager;

    while (isRunning) {
        // TODO: przerobic petle bo na szybzych procesorach program chodzi szybciej :D

        switch (State::CURRENT_APPLICATION_STATE) {
            case State::MAIN_MENU: // MAIN_MENU
                {
                    mainMenu();
                }
                break;

            case State::ACCOUNT_LOGIN: // ACCOUNT_LOGIN
                {
                    accountManager.loginAccount();
                }
                break;

            case State::ACCOUNT_REGISTER: // ACCOUNT_REGISTER
                {
                    accountManager.registerAccount();
                }
                break;

            case State::ACCOUNT_REGISTER_CUCCESS:
                {
                    accountManager.successRegister();
                }
                break;

            case State::ACCOUNT_LOGGED_MENU:
                {
                    accountManager.loggedMenu();
                }
                break;

            case State::ACCOUNT_MAKE_A_MONEY_TRANSFER:
                {
                    accountManager.makeMoneyTransfer();
                }
                break;

            case State::ACCOUNT_ADD_DEBET_CARD:
                {
                    accountManager.addDebetCard();
                }
                break;

            case State::ACCOUNT_DEBET_CARD_DEPOSIT_MONEY:
                {
                    accountManager.depositMoney();
                }
                break;

            case State::ACCOUNT_DEBET_CARD_WITHDRAW_MONEY:
                {
                    accountManager.withdrawMoeny();
                }
                break;

            case State::ACCOUNT_DEBET_CARD_BLOCK:
                {
                    accountManager.blockDebetCard();
                }
                break;

            case State::ACCOUNT_HISTORY:
                {
                    accountManager.history();
                }
                break;

            case State::ACCOUNT_CHANGE_PASSWORD:
                {
                    accountManager.changePassword();
                }
                break;

            case State::ACCOUNT_GET_CREDIT:
                {
                    accountManager.getCredit();
                }
                break;

            case State::EXIT: // EXIT
                {
                    isRunning = false;
                }
                break;
        }
    }
}

void mainMenu() {
    system("cls");

    cout << "Witaj w aplikacji bankowej!" << endl;
    cout << endl;
    cout << "Wybierz co chcesz zrobic:" << endl;
    cout << "[1] - Zaloguj sie:" << endl;
    cout << "[2] - Zarejestruj sie" << endl;
    cout << "[3] - Zresetuj haslo do konta" << endl;
    cout << "[4] - Wyjdz z aplikacji" << endl;

    int choice;
    cin >> choice;

    if (choice == 1) {
        State::CURRENT_APPLICATION_STATE = State::ACCOUNT_LOGIN;
    } else if (choice == 2) {
        State::CURRENT_APPLICATION_STATE = State::ACCOUNT_REGISTER;
    } else if (choice == 3) {
        State::CURRENT_APPLICATION_STATE = State::ACCOUNT_CHANGE_PASSWORD;
    } else if (choice == 4) {
        State::CURRENT_APPLICATION_STATE = State::EXIT;
    } else {
        State::CURRENT_APPLICATION_STATE = State::MAIN_MENU;
    }
}
