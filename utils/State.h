#include <iostream>
#include <vector>

using namespace std;

class State {
    public:
        static int CURRENT_APPLICATION_STATE;

        enum E_APPLICATION_STATES {
            MAIN_MENU,
            EXIT,
            ACCOUNT_LOGIN,
            ACCOUNT_REGISTER,
            ACCOUNT_LOGIN_SUCCESS,
            ACCOUNT_REGISTER_CUCCESS,
            ACCOUNT_LOGGED_MENU,
            ACCOUNT_MAKE_A_MONEY_TRANSFER,
            ACCOUNT_ADD_DEBET_CARD,
            ACCOUNT_DEBET_CARD_DEPOSIT_MONEY,
            ACCOUNT_DEBET_CARD_WITHDRAW_MONEY,
            ACCOUNT_DEBET_CARD_BLOCK,
            ACCOUNT_HISTORY,
            ACCOUNT_CHANGE_PASSWORD,
            ACCOUNT_GET_CREDIT
        };

        static E_APPLICATION_STATES APPLICATION_STATES;
};
