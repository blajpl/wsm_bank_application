#include <iostream>
#include <vector>

using namespace std;

namespace Utils {

    vector<string> explode(string, char);

	string generateStringByLength(string, int);

	string generateRandomString(string, int);

	string joinVectorString(vector<string>);

	string intToString(int);
}
