#include <iostream>
#include <vector>
#include <sstream>
#include <cmath>
#include <stdlib.h>

#include "Utils.h"

using namespace std;

namespace Utils {

	vector<string> explode(string str, char delimiter) {
		vector<string> arr;

		stringstream ss(str);
		string tok;

		while (getline(ss, tok, delimiter)) {
			arr.push_back(tok);
		}

		return arr;
	}

	string generateStringByLength(string str, int length) {
        string result;

        for (int i = 0; i < length; i++) {
            result += str[i];
        }

        return result;
	}

	string generateRandomString(string generateBase,int length) {
        string generatedChars;

        for (int i = 0; i < length; i++) {
            int tempInt = rand() % generateBase.length();
            generatedChars += generateBase[tempInt];
        }

        return generatedChars;
	}

	string joinVectorString(vector<string> vectorString) {
        string joinedString;

        for (int i = 0; i < vectorString.size(); i++) {
            joinedString += vectorString[i];

            if (i + 1 != vectorString.size()) {
                joinedString += ";";
            }
        }

        return joinedString;
	}

	string intToString(int integerToParse) {
        stringstream ss;
        ss << integerToParse;
        string result = ss.str();
        return result;
	}
}

