#include <iostream>
#include <stdlib.h>
#include "../database/Database.h"
#include "../utils/Utils.h"
#include "../utils/State.h"
#include "Account.h";
#include <ctime>

#include "AccountManager.h"

using namespace std;

const int ID_INDEX = 0;
const int USERNAME_INDEX = 1;
const int PASSWORD_INDEX = 2;
const int EMAIL_INDEX = 3;
const int FIRSTNAME_INDEX = 4;
const int LASTNAME_INDEX = 5;
const int PESEL_INDEX = 6;
const int ACCOUNT_NUMBER_INDEX = 7;
const int ACCOUNT_MONEY_AMMOUNT = 8;

const int DEBET_CARD_ID = 0;
const int DEBET_CARD_USER_ID = 1;
const int DEBET_CARD_PIN = 2;
const int DEBET_CARD_NUMBER  = 3;
const int DEBET_CARD_IS_BLOCKED = 4;
const int DEBET_CARD_NAME = 5;

const int HISTORY_ID = 0;
const int HISTORY_USER_ID = 1;
const int HISTORY_CONTENT = 2;
const int HISTORY_TYPE = 3;

const int HISTORY_DEPOSIT = 1;
const int HISTORY_WITHDRAW = 2;
const int HISTORY_TRANSFER = 3;
const int HISTORY_NEW_DEBET_CARD = 4;
const int HISTORY_BLOCK_CARD = 5;
const int HISTORY_CREDIT = 6;

int creditAmount = 0;
int creditMonth = 0;
int creditOdsetka = 0;
unsigned long creditTime = 0;

Account account;

void AccountManager::loginAccount() {
    string username;
    string password;

    bool successLogin = true;

    do {
        system("cls");

        if (!successLogin) {
            cout << "Podales niepoprawne dane do konta!" << endl;
            cout << "Jesli nie pamietasz hasla mozesz je sobie zresetowac." << endl << endl;
        }

        cout << "Logowanie do konta" << endl;
        cout << endl;

        cout << "Wpisz nazwe uzytkownika: ";
        cin >> username;
        cout << "Wpisz haslo: ";
        cin >> password;

        successLogin = false;
    } while (!existsByUsernameAndPassword(username, password));

    vector<string> existedAccount = findByUsername(username);

    account.currentLogged = true;
    account.id = atoi(existedAccount[ID_INDEX].c_str());
    account.username = existedAccount[USERNAME_INDEX];
    account.password = existedAccount[PASSWORD_INDEX];
    account.email = existedAccount[EMAIL_INDEX];
    account.firstname = existedAccount[FIRSTNAME_INDEX];
    account.lastname = existedAccount[LASTNAME_INDEX];
    account.pesel = existedAccount[PESEL_INDEX];
    account.accountNumber = existedAccount[ACCOUNT_NUMBER_INDEX];
    account.moneyAmmount = atoi(existedAccount[ACCOUNT_MONEY_AMMOUNT].c_str());

    State::CURRENT_APPLICATION_STATE = State::ACCOUNT_LOGGED_MENU;
}

void AccountManager::registerAccount() {
    string firstName;
    string lastName;
    string pesel;
    string email;
    string password;

    bool successRegister = true;

    do {
        system("cls");

        if (!successRegister) {
            cout << "Konto z takim peselem juz istnieje!" << endl << endl;
        }

        cout << "Rejestracja nowego konta" << endl;
        cout << endl;

        cout << "Wpisz imie: ";
        cin >> firstName;

        cout << "Wpisz nazwisko: ";
        cin >> lastName;

        cout << "Wpisz PESEL: ";
        cin >> pesel;

        cout << "Wpisz email: ";
        cin >> email;

        cout << "Wpisz haslo: ";
        cin >> password;

        successRegister = false;
    } while (existsByPesel(pesel));

    string username;

    do {
        string shortFirstName = Utils::generateStringByLength(firstName, 3);
        string shortLastName = Utils::generateStringByLength(lastName, 3);
        string randomNumbers = Utils::generateRandomString("0123456789", 4);
        username = shortFirstName + shortLastName + randomNumbers;
    } while (existsByUsername(username));

    string accountNumber;

    do {
        accountNumber = Utils::generateRandomString("0123456789", 16);
    } while (existsByAccountNumber(accountNumber));

    string newAccount = username + ";" + password + ";" + email + ";" + firstName + ";" + lastName + ";" + pesel + ";" + accountNumber + ";0";

    Database accountDb;
    accountDb.saveToFile("account.txt", newAccount);

    State::CURRENT_APPLICATION_STATE = State::ACCOUNT_REGISTER_CUCCESS;
}

void AccountManager::loggedMenu() {
    system("cls");

    cout << "Witaj: " << account.firstname << " " << account.lastname << endl;
    cout << "Twoj numer konta: " << account.accountNumber << endl;
    cout << "Posiadasz " << account.moneyAmmount << " pln" << endl;

    if (creditTime != 0) {
        cout << endl;
        cout << "Wziales kredy o wysokosci: " << creditAmount << " na " << creditMonth << " miesiecy." << endl;

        unsigned long currentTime = time(0);
        unsigned long diffTime = currentTime - creditTime;

        if (diffTime > 12) {
            creditOdsetka += 2;

            int odsetki = creditAmount * creditOdsetka/100;

            cout << "Posiadasz odsetki w wysokosci: " << odsetki << endl;
        }
    }

    cout << endl;
    cout << "Wybierz co chcesz zrobic:" << endl;
    cout << "[1] - Przelej pieniadze na rachunek obcy" << endl;
    cout << "[2] - Dodaj karte debetowa" << endl;
    cout << "[3] - Wyplac pieniadze" << endl;
    cout << "[4] - Wplac pieniadze" << endl;
    cout << "[5] - Zastrzez karte" << endl;
    cout << "[6] - Historia" << endl;
    cout << "[7] - Wez kredyt" << endl;
    cout << "[9] - Wyloguj sie" << endl;

    int choice;
    cin >> choice;

    if (choice == 9) {
        State::CURRENT_APPLICATION_STATE = State::MAIN_MENU;
    } else if (choice == 1) {
        State::CURRENT_APPLICATION_STATE = State::ACCOUNT_MAKE_A_MONEY_TRANSFER;
    } else if (choice == 2) {
        State::CURRENT_APPLICATION_STATE = State::ACCOUNT_ADD_DEBET_CARD;
    } else if (choice == 3) {
        State::CURRENT_APPLICATION_STATE = State::ACCOUNT_DEBET_CARD_WITHDRAW_MONEY;
    } else if (choice == 4) {
        State::CURRENT_APPLICATION_STATE = State::ACCOUNT_DEBET_CARD_DEPOSIT_MONEY;
    } else if (choice == 5) {
        State::CURRENT_APPLICATION_STATE = State::ACCOUNT_DEBET_CARD_BLOCK;
    } else if (choice == 6) {
        State::CURRENT_APPLICATION_STATE = State::ACCOUNT_HISTORY;
    } else if (choice == 7) {
        State::CURRENT_APPLICATION_STATE = State::ACCOUNT_GET_CREDIT;
    } else {
        State::CURRENT_APPLICATION_STATE = State::ACCOUNT_LOGGED_MENU;
    }
}

void AccountManager::makeMoneyTransfer() {
    string accountNumber;

    do {
        system("cls");
        cout << "Wpisz numer konta na jaki chcesz zrobic przelew:" << endl;
        cin >> accountNumber;
    } while (!existsByAccountNumber(accountNumber));

    int moneyAmmount;

    do {
        system("cls");
        cout << "Wpisz ilosc pieniedzy do przelewu:" << endl;
        cin >> moneyAmmount;
    } while (!isEnoughMoney(moneyAmmount));

    Database accountDb;

    account.moneyAmmount -= moneyAmmount;
    string updateLoggedAccount =
        Utils::intToString(account.id) + ";" + account.username + ";" + account.password + ";" + account.email + ";" +
        account.firstname + ";" + account.lastname+ ";" + account.pesel + ";" +
        account.accountNumber + ";" + Utils::intToString(account.moneyAmmount);

    vector<string> otherAccountVector = findByAccountNumber(accountNumber);

    Account otherAccount;
    otherAccount.id = atoi(otherAccountVector[ID_INDEX].c_str());
    otherAccount.username = otherAccountVector[USERNAME_INDEX];
    otherAccount.password = otherAccountVector[PASSWORD_INDEX];
    otherAccount.email = otherAccountVector[EMAIL_INDEX];
    otherAccount.firstname = otherAccountVector[FIRSTNAME_INDEX];
    otherAccount.lastname = otherAccountVector[LASTNAME_INDEX];
    otherAccount.pesel = otherAccountVector[PESEL_INDEX];
    otherAccount.accountNumber = otherAccountVector[ACCOUNT_NUMBER_INDEX];
    otherAccount.moneyAmmount = atoi(otherAccountVector[ACCOUNT_MONEY_AMMOUNT].c_str()) + moneyAmmount;

    string updateOtherAccount =
        Utils::intToString(otherAccount.id) + ";" + otherAccount.username + ";" + otherAccount.password + ";" + otherAccount.email + ";" +
        otherAccount.firstname + ";" + otherAccount.lastname+ ";" + otherAccount.pesel + ";" +
        otherAccount.accountNumber + ";" + Utils::intToString(otherAccount.moneyAmmount);

    accountDb.replaceLineInFile("account.txt", updateLoggedAccount, account.id);
    accountDb.replaceLineInFile("account.txt", updateOtherAccount, otherAccount.id);

    string historyString = Utils::intToString(account.id) + ";Zostalo przelane " + Utils::intToString(moneyAmmount) + " na konto " + otherAccount.firstname + " " + otherAccount.lastname + ";" + Utils::intToString(HISTORY_TRANSFER);

    Database historyDb;
    historyDb.saveToFile("history.txt", historyString);

    system("cls");

    cout << "Przelales " << moneyAmmount << endl;
    cout << "Na konto odbiorcy: " << endl;
    cout << otherAccount.firstname << otherAccount.lastname << endl;

    cout << endl;
    cout << "Wybierz co chcesz zrobic:" << endl;
    cout << "[1] - Zrob ponowanie przelew" << endl;
    cout << "[2] - Przejdz do glownego menu" << endl;

    int choice;
    cin >> choice;

    if (choice == 1) {
        State::CURRENT_APPLICATION_STATE = State::ACCOUNT_MAKE_A_MONEY_TRANSFER;
    } else {
        State::CURRENT_APPLICATION_STATE = State::ACCOUNT_LOGGED_MENU;
    }
}

void AccountManager::addDebetCard() {
    string pinNumber;

    bool isSuccess = true;
    do {
        system("cls");

        if (!isSuccess) {
            cout << "Pin powinien zawierac 4 cyfry!" << endl;
        }

        cout << "Podaj pin do karty:" << endl;
        cin >> pinNumber;

        isSuccess = false;
    } while(pinNumber.length() != 4);

    string debetCardName;

    isSuccess = true;
    do {
        system("cls");

        if (!isSuccess) {
            cout << "Nazwa karty powinna minimum 4 znaki!" << endl;
        }

        cout << "Podaj nazwe karty:" << endl;
        cin >> debetCardName;

        isSuccess = false;
    } while (debetCardName.length() < 4);

    string debetCardNumber;

    do {
        debetCardNumber = Utils::generateRandomString("0123456789", 16);
    } while (existsDebetCardNumber(debetCardNumber));

    string debetCardString = Utils::intToString(account.id) + ";" + pinNumber + ";" + debetCardNumber + ";0;" + debetCardName;

    Database debetCardDb;
    debetCardDb.saveToFile("debet_card.txt", debetCardString);

    string historyString = Utils::intToString(account.id ) + ";Dodano nowa karte debetowa do konta o nazwie " + debetCardName + ";" + Utils::intToString(HISTORY_NEW_DEBET_CARD);

    Database historyDb;
    historyDb.saveToFile("history.txt", historyString);

    system("cls");

    cout << "Dodales karte debetowa pomyslnie!" << endl;
    cout << endl;
    cout << "Wybierz co chcesz zrobic:" << endl;
    cout << "[1] - Dodaj kolejna" << endl;
    cout << "[2] - Przejdz do glownego menu" << endl;

    int choice;
    cin >> choice;

    if (choice == 1) {
        State::CURRENT_APPLICATION_STATE = State::ACCOUNT_ADD_DEBET_CARD;
    } else {
        State::CURRENT_APPLICATION_STATE = State::ACCOUNT_LOGGED_MENU;
    }
}

void AccountManager::depositMoney() {
    vector< vector<string> > debetCards = findAllDebetCardByUserId(account.id);

    system("cls");

    cout << "Wybierz karte, ktora chcesz wplacic pieniadze!" << endl;

    for (int i = 0; i < debetCards.size(); i++) {
        string isBlocked = "NIE";

        if (atoi(debetCards[i][DEBET_CARD_IS_BLOCKED].c_str()) == 1) {
            isBlocked = "TAK";
        }

        cout << "[" << debetCards[i][DEBET_CARD_ID] << "] - " << debetCards[i][DEBET_CARD_NAME] << " ~ " << debetCards[i][DEBET_CARD_NUMBER] << ", zastrzezona: " << isBlocked << endl;
    }

    int debetCardId;

    bool isSuccess = true;
    do {
       if (!isSuccess) {
            cout << "Nie posiadasz takiej karty lub karta jest zastrzezona!" << endl;
            cout << "Wybierz ponownie!" << endl;
        }

        cin >> debetCardId;

        isSuccess = false;
    } while(!existsDebetCardId(debetCardId, account.id) || isDebetCardBlocked(debetCardId));

    system("cls");

    cout << "Wpisz pin do karty:" << endl;

    int pin;

    isSuccess = true;
    do {
        if (!isSuccess) {
            cout << "Bledny pin do karty" << endl;
            cout << "Wpisz ponownie!" << endl;
        }

        cin >> pin;

        isSuccess = false;
    } while (!isDebetCardPinOk(debetCardId, pin));

    system("cls");

    cout << "Wpisz ilosc pieniedzy do wplacenia:" << endl;

    int moneyAmmount;
    cin >> moneyAmmount;

    Database accountDb;

    account.moneyAmmount += moneyAmmount;
    string updateLoggedAccount =
        Utils::intToString(account.id) + ";" + account.username + ";" + account.password + ";" + account.email + ";" +
        account.firstname + ";" + account.lastname+ ";" + account.pesel + ";" +
        account.accountNumber + ";" + Utils::intToString(account.moneyAmmount);

    accountDb.replaceLineInFile("account.txt", updateLoggedAccount, account.id);

    string historyString = Utils::intToString(account.id ) + ";Wplacil " + Utils::intToString(moneyAmmount) + " pieniedzy" + ";" + Utils::intToString(HISTORY_DEPOSIT);

    Database historyDb;
    historyDb.saveToFile("history.txt", historyString);

    system("cls");

    cout << "Wplaciles " << moneyAmmount << endl;

    cout << endl;
    cout << "Wybierz co chcesz zrobic:" << endl;
    cout << "[1] - Wplac ponownie" << endl;
    cout << "[2] - Przejdz do glownego menu" << endl;

    int choice;
    cin >> choice;

    if (choice == 1) {
        State::CURRENT_APPLICATION_STATE = State::ACCOUNT_DEBET_CARD_DEPOSIT_MONEY;
    } else {
        State::CURRENT_APPLICATION_STATE = State::ACCOUNT_LOGGED_MENU;
    }
}

void AccountManager::withdrawMoeny() {
    vector< vector<string> > debetCards = findAllDebetCardByUserId(account.id);

    system("cls");

    cout << "Wybierz karte, ktora chcesz wyplacic pieniadze!" << endl;

    for (int i = 0; i < debetCards.size(); i++) {
        string isBlocked = "NIE";

        if (atoi(debetCards[i][DEBET_CARD_IS_BLOCKED].c_str()) == 1) {
            isBlocked = "TAK";
        }

        cout << "[" << debetCards[i][DEBET_CARD_ID] << "] - " << debetCards[i][DEBET_CARD_NAME] << " ~ " << debetCards[i][DEBET_CARD_NUMBER] << ", zastrzezona: " << isBlocked << endl;
    }

    int debetCardId;

    bool isSuccess = true;
    do {
       if (!isSuccess) {
            cout << "Nie posiadasz takiej karty lub karta jest zastrzezona!" << endl;
            cout << "Wybierz ponownie!" << endl;
        }

        cin >> debetCardId;

        isSuccess = false;
    } while(!existsDebetCardId(debetCardId, account.id) || isDebetCardBlocked(debetCardId));

    system("cls");

    cout << "Wpisz pin do karty:" << endl;

    int pin;

    isSuccess = true;
    do {
        if (!isSuccess) {
            cout << "Bledny pin do karty" << endl;
            cout << "Wpisz ponownie!" << endl;
        }

        cin >> pin;

        isSuccess = false;
    } while (!isDebetCardPinOk(debetCardId, pin));

    system("cls");

    cout << "Wpisz ilosc pieniedzy do wyplacenia:" << endl;

    int moneyAmmount;

    isSuccess = true;
    do {
        if (!isSuccess) {
            cout << "Nie posiadasz tyle pieniedzy" << endl;
            cout << "Wpisz ponownie!" << endl;
        }

        cin >> moneyAmmount;

        isSuccess = false;
    } while (!isEnoughMoney(moneyAmmount));

    Database accountDb;

    account.moneyAmmount -= moneyAmmount;
    string updateLoggedAccount =
        Utils::intToString(account.id) + ";" + account.username + ";" + account.password + ";" + account.email + ";" +
        account.firstname + ";" + account.lastname+ ";" + account.pesel + ";" +
        account.accountNumber + ";" + Utils::intToString(account.moneyAmmount);

    accountDb.replaceLineInFile("account.txt", updateLoggedAccount, account.id);

    string historyString = Utils::intToString(account.id) + ";Wyplacil " + Utils::intToString(moneyAmmount) + " pieniedzy" + ";" + Utils::intToString(HISTORY_WITHDRAW);

    Database historyDb;
    historyDb.saveToFile("history.txt", historyString);

    system("cls");

    cout << "Wyplaciles " << moneyAmmount << endl;

    cout << endl;
    cout << "Wybierz co chcesz zrobic:" << endl;
    cout << "[1] - Wplac ponownie" << endl;
    cout << "[2] - Przejdz do glownego menu" << endl;

    int choice;
    cin >> choice;

    if (choice == 1) {
        State::CURRENT_APPLICATION_STATE = State::ACCOUNT_DEBET_CARD_WITHDRAW_MONEY;
    } else {
        State::CURRENT_APPLICATION_STATE = State::ACCOUNT_LOGGED_MENU;
    }
}

void AccountManager::blockDebetCard() {
    vector< vector<string> > debetCards = findAllDebetCardByUserId(account.id);

    system("cls");

    cout << "Wybierz karte, ktora chcesz zastrzec!" << endl;

    for (int i = 0; i < debetCards.size(); i++) {
        string isBlocked = "NIE";

        if (atoi(debetCards[i][DEBET_CARD_IS_BLOCKED].c_str()) == 1) {
            isBlocked = "TAK";
        }

        cout << "[" << debetCards[i][DEBET_CARD_ID] << "] - " << debetCards[i][DEBET_CARD_NAME] << " ~ " << debetCards[i][DEBET_CARD_NUMBER] << ", zastrzezona: " << isBlocked << endl;
    }

    int debetCardId;

    bool isSuccess = true;
    do {
       if (!isSuccess) {
            cout << "Nie posiadasz takiej karty lub karta jest juz zastrzezona!" << endl;
            cout << "Wybierz ponownie!" << endl;
        }

        cin >> debetCardId;

        isSuccess = false;
    } while(!existsDebetCardId(debetCardId, account.id) || isDebetCardBlocked(debetCardId));

    system("cls");

    cout << "Wpisz pin do karty:" << endl;

    int pin;

    isSuccess = true;
    do {
        if (!isSuccess) {
            cout << "Bledny pin do karty" << endl;
            cout << "Wpisz ponownie!" << endl;
        }

        cin >> pin;

        isSuccess = false;
    } while (!isDebetCardPinOk(debetCardId, pin));

    vector<string> debetCardVector = findDebetCardById(debetCardId);

    string userId = debetCardVector[DEBET_CARD_USER_ID];
    string debetCardNumber = debetCardVector[DEBET_CARD_NUMBER];
    string debetCardPin = debetCardVector[DEBET_CARD_PIN];
    string debetCardName = debetCardVector[DEBET_CARD_NAME];

    string updateDebetCard =
        Utils::intToString(debetCardId) + ";" + Utils::intToString(account.id) + ";" +
        debetCardPin + ";" + debetCardNumber + ";1;" + debetCardName;

    Database debetCardDb;

    debetCardDb.replaceLineInFile("debet_card.txt", updateDebetCard, debetCardId);

    string historyString = Utils::intToString(account.id ) + ";Zablokowano karte " + debetCardName + " o numerze " + debetCardNumber + ";" + Utils::intToString(HISTORY_BLOCK_CARD);

    Database historyDb;
    historyDb.saveToFile("history.txt", historyString);

    cout << endl;
    cout << "Wybierz co chcesz zrobic:" << endl;
    cout << "[1] - Zablokuj inna karte" << endl;
    cout << "[2] - Przejdz do glownego menu" << endl;

    int choice;
    cin >> choice;

    if (choice == 1) {
        State::CURRENT_APPLICATION_STATE = State::ACCOUNT_DEBET_CARD_BLOCK;
    } else {
        State::CURRENT_APPLICATION_STATE = State::ACCOUNT_LOGGED_MENU;
    }
}

void AccountManager::successRegister() {
    system("cls");

    cout << "Zarejestrowales swoje konto pomyslnie!" << endl;
    cout << endl;
    cout << "Wybierz co chcesz zrobic:" << endl;
    cout << "[1] - Zaloguj sie" << endl;
    cout << "[2] - Przejdz do glownego menu" << endl;

    int choice;
    cin >> choice;

    if (choice == 1) {
        State::CURRENT_APPLICATION_STATE = State::ACCOUNT_LOGIN;
    } else {
        State::CURRENT_APPLICATION_STATE = State::MAIN_MENU;
    }
}

void AccountManager::history() {
    system("cls");

    cout << "Wybierz filtr: " << endl;
    cout << "[1] - Brak" << endl;
    cout << "[2] - Przelewy" << endl;
    cout << "[3] - Wplata pieniedzy" << endl;
    cout << "[4] - Wyplata piniedzy" << endl;
    cout << "[5] - Dodanie nowej karty" << endl;
    cout << "[6] - Zastrzezenie karty" << endl;
    cout << "[7] - Wziete kredyty" << endl;

    int type = -1;

    int choice;
    cin >> choice;

    switch (choice) {
        case 1:
            type = -1;
            break;

        case 2:
            type = HISTORY_TRANSFER;
            break;

        case 3:
            type = HISTORY_DEPOSIT;
            break;

        case 4:
            type = HISTORY_WITHDRAW;
            break;

        case 5:
            type = HISTORY_NEW_DEBET_CARD;
            break;

        case 6:
            type = HISTORY_BLOCK_CARD;
            break;

        case 7:
            type = HISTORY_CREDIT;
            break;
    }

    system("cls");

    vector< vector<string> > historys = findAllHistoryByUserIdAndType(account.id, type);

    if (historys.size() == 0){
        cout << "Brak historii!" << endl;
    } else {
        cout << "Historia: " << endl;

        for (int i = 0; i < historys.size(); i++) {
            cout << historys[i][HISTORY_CONTENT] << endl;
        }
    }

    cout << endl;
    cout << "Wybierz co chcesz zrobic:" << endl;
    cout << "[1] - Wejdz ponownie w historie" << endl;
    cout << "[2] - Przejdz do glownego menu" << endl;

    cin >> choice;

    if (choice == 1) {
        State::CURRENT_APPLICATION_STATE = State::ACCOUNT_HISTORY;
    } else {
        State::CURRENT_APPLICATION_STATE = State::ACCOUNT_LOGGED_MENU;
    }
}

void AccountManager::changePassword() {
    string login;
    string email;

    bool isSuccess = true;
    do {
        system("cls");

        cout << "Resetowanie hasla" << endl;
        cout << endl;

        if (!isSuccess) {
            cout << "Konto o podanym loginie i mailu nie istnieje!" << endl << endl;
        }

        cout << "Wpisz login" << endl;
        cin >> login;

        cout << "Wpisz mail" << endl;
        cin >> email;

        isSuccess = false;
    } while(findByUsernameAndEmail(login, email).size() == 0);

    string newPassword = Utils::generateRandomString("qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890", 8);

    vector<string> otherAccountVector = findByUsernameAndEmail(login, email);

    Account otherAccount;
    otherAccount.id = atoi(otherAccountVector[ID_INDEX].c_str());
    otherAccount.username = otherAccountVector[USERNAME_INDEX];
    otherAccount.password = otherAccountVector[PASSWORD_INDEX];
    otherAccount.email = otherAccountVector[EMAIL_INDEX];
    otherAccount.firstname = otherAccountVector[FIRSTNAME_INDEX];
    otherAccount.lastname = otherAccountVector[LASTNAME_INDEX];
    otherAccount.pesel = otherAccountVector[PESEL_INDEX];
    otherAccount.accountNumber = otherAccountVector[ACCOUNT_NUMBER_INDEX];
    otherAccount.moneyAmmount = atoi(otherAccountVector[ACCOUNT_MONEY_AMMOUNT].c_str());

    string updateOtherAccount =
        Utils::intToString(otherAccount.id) + ";" + otherAccount.username + ";" + newPassword + ";" + otherAccount.email + ";" +
        otherAccount.firstname + ";" + otherAccount.lastname+ ";" + otherAccount.pesel + ";" +
        otherAccount.accountNumber + ";" + Utils::intToString(otherAccount.moneyAmmount);

    Database accountDb;
    accountDb.replaceLineInFile("account.txt", updateOtherAccount, otherAccount.id);

    cout << "Haslo zostalo zresetowane!" << endl;
    cout << "Nowe haslo: " << newPassword << endl;
    cout << endl;
    cout << "Wybierz co chcesz zrobic:" << endl;
    cout << "[1] - Zresetuj haslo ponownie" << endl;
    cout << "[2] - Przejdz do glownego menu" << endl;

    int choice;
    cin >> choice;

    if (choice == 1) {
        State::CURRENT_APPLICATION_STATE = State::ACCOUNT_CHANGE_PASSWORD;
    } else {
        State::CURRENT_APPLICATION_STATE = State::MAIN_MENU;
    }
}

void AccountManager::getCredit() {
    system("cls");
    cout << "Branie kredytu" << endl;
    cout << endl;

    cout << "Wpisz ile chcesz wziac kredytu:" << endl;
    int credit;
    cin >> credit;

    cout << "Wpisz na ile miesiecy: " << endl;
    int month;
    cin >> month;

    if (creditTime != 0) {
        cout << "Masz juz wziety jeden kredyt!!!" << endl;
    } else {
        creditTime = time(0);
        creditAmount = credit;
        creditMonth = month;

        Database accountDb;

        account.moneyAmmount += credit;
        string updateLoggedAccount =
            Utils::intToString(account.id) + ";" + account.username + ";" + account.password + ";" + account.email + ";" +
            account.firstname + ";" + account.lastname+ ";" + account.pesel + ";" +
            account.accountNumber + ";" + Utils::intToString(account.moneyAmmount);

        accountDb.replaceLineInFile("account.txt", updateLoggedAccount, account.id);

        string historyString = Utils::intToString(account.id) + ";Wzieto kredyt o wysokosci " + Utils::intToString(credit) + " na " + Utils::intToString(month) + " miesiecy" + ";" + Utils::intToString(HISTORY_CREDIT);

        Database historyDb;
        historyDb.saveToFile("history.txt", historyString);
    }

    cout << "Wybierz co chcesz zrobic:" << endl;
    cout << "[1] - Przejdz do glownego menu" << endl;

    int choice;
    cin >> choice;

    if (choice == 1) {
        State::CURRENT_APPLICATION_STATE = State::ACCOUNT_LOGGED_MENU;
    } else {
        State::CURRENT_APPLICATION_STATE = State::ACCOUNT_LOGGED_MENU;
    }
}

bool AccountManager::existsByUsernameAndPassword(string username, string password) {
    Database accountDb;

    vector< vector<string> > accountArray = accountDb.readFile("account.txt");

    for (int i = 0; i < accountArray.size(); i++) {
        if (accountArray[i][USERNAME_INDEX] == username && accountArray[i][PASSWORD_INDEX] == password) {
            return true;
        }
    }

    return false;
}

bool AccountManager::existsByPesel(string pesel) {
    Database accountDb;

    vector< vector<string> > accountArray = accountDb.readFile("account.txt");

    for (int i = 0; i < accountArray.size(); i++) {
        if (accountArray[i][PESEL_INDEX] == pesel) {
            return true;
        }
    }

    return false;
}

bool AccountManager::existsByUsername(string username) {
    Database accountDb;

    vector< vector<string> > accountArray = accountDb.readFile("account.txt");

    for (int i = 0; i < accountArray.size(); i++) {
        if (accountArray[i][USERNAME_INDEX] == username) {
            return true;
        }
    }

    return false;
}

bool AccountManager::existsByAccountNumber(string accountNumber) {
    Database accountDb;

    vector< vector<string> > accountArray = accountDb.readFile("account.txt");

    for (int i = 0; i < accountArray.size(); i++) {
        if (accountArray[i][ACCOUNT_NUMBER_INDEX] == accountNumber) {
            return true;
        }
    }

    return false;
}

bool AccountManager::isEnoughMoney(int ammountMoney) {
    Database accountDb;

    vector< vector<string> > accountArray = accountDb.readFile("account.txt");

    for (int i = 0; i < accountArray.size(); i++) {
        if (atoi(accountArray[i][ACCOUNT_MONEY_AMMOUNT].c_str()) >= ammountMoney) {
            return true;
        }
    }

    return false;
}

bool AccountManager::existsDebetCardNumber(string debetCardNumber) {
    Database debetCardDb;

    vector< vector<string> > debetCardArray = debetCardDb.readFile("debet_card.txt");

    for (int i = 0; i < debetCardArray.size(); i++) {
        if (debetCardArray[i][DEBET_CARD_NUMBER] == debetCardNumber) {
            return true;
        }
    }

    return false;
}

bool AccountManager::existsDebetCardId(int debetCardId, int accountId) {
    Database debetCardDb;

    vector< vector<string> > debetCardArray = debetCardDb.readFile("debet_card.txt");

    for (int i = 0; i < debetCardArray.size(); i++) {
        if (atoi(debetCardArray[i][DEBET_CARD_ID].c_str()) == debetCardId && atoi(debetCardArray[i][DEBET_CARD_USER_ID].c_str()) == accountId) {
            return true;
        }
    }

    return false;
}

bool AccountManager::isDebetCardPinOk(int debetCardId, int pin) {
    Database debetCardDb;

    vector< vector<string> > debetCardArray = debetCardDb.readFile("debet_card.txt");

    for (int i = 0; i < debetCardArray.size(); i++) {
        if (atoi(debetCardArray[i][DEBET_CARD_ID].c_str()) == debetCardId && atoi(debetCardArray[i][DEBET_CARD_PIN].c_str()) == pin) {
            return true;
        }
    }

    return false;
}

bool AccountManager::isDebetCardBlocked(int debetCardId) {
    Database debetCardDb;

    vector< vector<string> > debetCardArray = debetCardDb.readFile("debet_card.txt");

    for (int i = 0; i < debetCardArray.size(); i++) {
        if (atoi(debetCardArray[i][DEBET_CARD_ID].c_str()) == debetCardId && atoi(debetCardArray[i][DEBET_CARD_IS_BLOCKED].c_str()) == 1) {
            return true;
        }
    }

    return false;
}

vector<string> AccountManager::findByUsername(string username) {
    Database accountDb;

    vector< vector<string> > accountArray = accountDb.readFile("account.txt");

    vector<string> result;

    for (int i = 0; i < accountArray.size(); i++) {
        if (accountArray[i][USERNAME_INDEX] == username) {
            result = accountArray[i];
        }
    }

    return result;
}

vector<string> AccountManager::findByUsernameAndEmail(string username, string email) {
    Database accountDb;

    vector< vector<string> > accountArray = accountDb.readFile("account.txt");

    vector<string> result;

    for (int i = 0; i < accountArray.size(); i++) {
        if (accountArray[i][USERNAME_INDEX] == username && accountArray[i][EMAIL_INDEX] == email) {
            result = accountArray[i];
        }
    }

    return result;
}

vector<string> AccountManager::findByAccountNumber(string accountNumber) {
    Database accountDb;

    vector< vector<string> > accountArray = accountDb.readFile("account.txt");

    vector<string> result;

    for (int i = 0; i < accountArray.size(); i++) {
        if (accountArray[i][ACCOUNT_NUMBER_INDEX] == accountNumber) {
            result = accountArray[i];
        }
    }

    return result;
}

vector< vector<string> > AccountManager::findAllDebetCardByUserId(int userId) {
    Database debetCardDb;

    vector< vector<string> > debetCardArray = debetCardDb.readFile("debet_card.txt");

    vector< vector<string> > result;

    for (int i = 0; i < debetCardArray.size(); i++) {
        if (atoi(debetCardArray[i][DEBET_CARD_USER_ID].c_str()) == userId) {
            result.push_back(debetCardArray[i]);
        }
    }

    return result;
}

vector< vector<string> > AccountManager::findAllHistoryByUserIdAndType(int userId, int type) {
    Database historyDb;

    vector< vector<string> > historyArray = historyDb.readFile("history.txt");

    vector< vector<string> > result;

    for (int i = 0; i < historyArray.size(); i++) {
        if (type == -1) {
            if (atoi(historyArray[i][HISTORY_USER_ID].c_str()) == userId) {
                result.push_back(historyArray[i]);
            }
        } else {
            if (atoi(historyArray[i][HISTORY_USER_ID].c_str()) == userId && atoi(historyArray[i][HISTORY_TYPE].c_str()) == type) {
                result.push_back(historyArray[i]);
            }
        }
    }

    return result;
}

vector<string> AccountManager::findDebetCardById(int debetCardId) {
    Database debetCardDb;

    vector< vector<string> > debetCardArray = debetCardDb.readFile("debet_card.txt");

    vector<string> result;

    for (int i = 0; i < debetCardArray.size(); i++) {
        if (atoi(debetCardArray[i][DEBET_CARD_ID].c_str()) == debetCardId) {
            result = debetCardArray[i];
        }
    }

    return result;
}
