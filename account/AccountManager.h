#include <iostream>
#include <vector>

using namespace std;

class AccountManager {

    private:
        bool existsByUsernameAndPassword(string, string);
        bool existsByPesel(string);
        bool existsByUsername(string);
        bool existsByAccountNumber(string);
        bool existsDebetCardNumber(string);
        bool isEnoughMoney(int);
        bool existsDebetCardId(int, int);
        bool isDebetCardPinOk(int, int);
        bool isDebetCardBlocked(int);
        vector<string> findByUsername(string);
        vector<string> findByUsernameAndEmail(string, string);
        vector<string> findByAccountNumber(string);
        vector<string> findDebetCardById(int);
        vector< vector<string> > findAllDebetCardByUserId(int);
        vector< vector<string> > findAllHistoryByUserIdAndType(int, int);

    public:
        void registerAccount();
        void loginAccount();
        void loggedMenu();
        void successRegister();
        void makeMoneyTransfer();
        void addDebetCard();
        void depositMoney();
        void withdrawMoeny();
        void blockDebetCard();
        void history();
        void changePassword();
        void getCredit();
};
