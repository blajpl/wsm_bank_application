#include <iostream>
#include <vector>
#include <fstream>
#include <stdlib.h>
#include "../utils/Utils.h"

#include "Database.h"

using namespace std;

const int ID_INDEX = 0;

vector< vector<string> > Database::readFile(string fileName) {
    ifstream file(fileName.c_str(), ios::app);

	if (!file.good()) {
		cout << "Nie mozna otworzyc pliku!" << endl;
	}

	string line;

	vector< vector<string> > content;

	while (getline(file, line)) {
		vector<string> explodedLine = Utils::explode(line, ';');

		vector<string> item;

		for (int i = 0; i< explodedLine.size(); i++) {
			item.push_back(explodedLine[i]);
		}

		content.push_back(item);
	}

	file.close();

	return content;
}

void Database::saveToFile(string fileName, string content) {
    ofstream file(fileName.c_str(), ios::out | ios::app);
    int generatedId = generateID(fileName);

    if (file.is_open()) {
        file << generatedId << ";" << content << endl;
        file.close();
    }
}

void Database::replaceLineInFile(string fileName, string content, int id) {
    vector< vector<string> > oldContent = readFile(fileName);
    vector<string> newContent;

    for (int i = 0; i < oldContent.size(); i++) {
        if (atoi(oldContent[i][ID_INDEX].c_str()) == id) {
            newContent.push_back(content);
        } else {
            string contentToReplace = Utils::joinVectorString(oldContent[i]);
            newContent.push_back(contentToReplace);
        }
    }

    ofstream file(fileName.c_str(), ios::trunc);

    if (file.is_open()) {
        for (int i = 0; i < newContent.size(); i++) {
            file << newContent[i] << endl;
        }

        file.close();
    }
}

int Database::generateID(string fileName) {
    vector< vector<string> > accountArray = readFile(fileName);

    int lastId = 0;

    if (accountArray.size() != 0) {
        lastId = atoi(accountArray[accountArray.size() - 1][ID_INDEX].c_str());
    }

    return lastId + 1;
}
