#include <iostream>
#include <vector>
#include <fstream>

using namespace std;

class Database {

	private:
		int generateID(string);

	public:
		vector< vector<string> > readFile(string);
		void saveToFile(string, string);
		void replaceLineInFile(string, string, int);
};
